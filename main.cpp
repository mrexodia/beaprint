#include <stdio.h>
#include <windows.h>
#include "BeaEngine\BeaEngine.h"

#define TYPE_VALUE 1
#define TYPE_MEMORY 2
#define TYPE_ADDR 4

#define MAX_MNEMONIC_SIZE 64

enum MEMORY_SIZE
{
    size_byte,
    size_word,
    size_dword,
    size_qword
};

typedef MEMORY_SIZE VALUE_SIZE;

struct MEMORY_INFO
{
    ULONG_PTR value; //displacement / addrvalue (rip-relative)
    MEMORY_SIZE size; //byte/word/dword/qword
    char mnemonic[MAX_MNEMONIC_SIZE];
};

struct VALUE_INFO
{
    ULONG_PTR value;
    VALUE_SIZE size;
};

struct BASIC_INSTRUCTION_INFO
{
    DWORD type; //value|memory|addr
    VALUE_INFO value; //immediat
    MEMORY_INFO memory;
    ULONG_PTR addr; //addrvalue (jumps + calls)
};

static void printmemoryinfo(MEMORY_INFO* meminfo)
{
    printf(" mnemonic: \"%s\"\n", meminfo->mnemonic);
    switch(meminfo->size)
    {
    case size_byte:
        puts("     size: byte");
        break;
    case size_word:
        puts("     size: word");
        break;
    case size_dword:
        puts("     size: dword");
        break;
    case size_qword:
        puts("     size: qword");
        break;
    }
    printf("    value: %p\n", meminfo->value);
}

static void printvalueinfo(VALUE_INFO* valinfo)
{
    switch(valinfo->size)
    {
    case size_byte:
        puts("     size: byte");
        break;
    case size_word:
        puts("     size: word");
        break;
    case size_dword:
        puts("     size: dword");
        break;
    case size_qword:
        puts("     size: qword");
        break;
    }
    printf("    value: %p\n", valinfo->value);
}

static void printbasicinfo(BASIC_INSTRUCTION_INFO* basicinfo)
{
    if((basicinfo->type&TYPE_VALUE)==TYPE_VALUE)
    {
        puts(" value:");
        printvalueinfo(&basicinfo->value);
    }
    if((basicinfo->type&TYPE_MEMORY)==TYPE_MEMORY)
    {
        puts("memory:");
        printmemoryinfo(&basicinfo->memory);
    }
    if((basicinfo->type&TYPE_ADDR)==TYPE_ADDR)
    {
        printf("  addr: %p\n", basicinfo->addr);
    }
}

static MEMORY_SIZE argsize2memsize(int argsize)
{
    switch(argsize)
    {
    case 8:
        return size_byte;
    case 16:
        return size_word;
    case 32:
        return size_dword;
    case 64:
        return size_qword;
    }
    return size_byte;
}

static void fillbasicinfo(DISASM* disasm, BASIC_INSTRUCTION_INFO* basicinfo)
{
    //set type to zero
    basicinfo->type=0;
    //find immidiat
    if(disasm->Instruction.BranchType==0) //no branch
    {
        if((disasm->Argument1.ArgType&CONSTANT_TYPE)==CONSTANT_TYPE)
        {
            basicinfo->type|=TYPE_VALUE;
            basicinfo->value.value=(ULONG_PTR)disasm->Instruction.Immediat;
            basicinfo->value.size=argsize2memsize(disasm->Argument1.ArgSize);
        }
        else if((disasm->Argument2.ArgType&CONSTANT_TYPE)==CONSTANT_TYPE)
        {
            basicinfo->type|=TYPE_VALUE;
            basicinfo->value.value=(ULONG_PTR)disasm->Instruction.Immediat;
            basicinfo->value.size=argsize2memsize(disasm->Argument2.ArgSize);
        }
    }
    //find memory displacement
    if((disasm->Argument1.ArgType&MEMORY_TYPE)==MEMORY_TYPE || (disasm->Argument2.ArgType&MEMORY_TYPE)==MEMORY_TYPE)
    {
        if(disasm->Argument1.Memory.Displacement)
        {
            basicinfo->type|=TYPE_MEMORY;
            basicinfo->memory.value=(ULONG_PTR)disasm->Argument1.Memory.Displacement;
            strcpy(basicinfo->memory.mnemonic, disasm->Argument1.ArgMnemonic);
            basicinfo->memory.size=argsize2memsize(disasm->Argument1.ArgSize);
        }
        else if(disasm->Argument2.Memory.Displacement)
        {
            basicinfo->type|=TYPE_MEMORY;
            basicinfo->memory.value=(ULONG_PTR)disasm->Argument2.Memory.Displacement;
            strcpy(basicinfo->memory.mnemonic, disasm->Argument2.ArgMnemonic);
            basicinfo->memory.size=argsize2memsize(disasm->Argument2.ArgSize);
        }
    }
    //find address value
    if(disasm->Instruction.BranchType && disasm->Instruction.AddrValue)
    {
        basicinfo->type|=TYPE_ADDR;
        basicinfo->addr=(ULONG_PTR)disasm->Instruction.AddrValue;
    }
    //rip-relative (non-branch)
    if(disasm->Instruction.BranchType==0)
    {
        if((disasm->Argument1.ArgType&RELATIVE_)==RELATIVE_)
        {
            basicinfo->type|=TYPE_MEMORY;
            basicinfo->memory.value=(ULONG_PTR)disasm->Instruction.AddrValue;
            strcpy(basicinfo->memory.mnemonic, disasm->Argument1.ArgMnemonic);
            basicinfo->memory.size=argsize2memsize(disasm->Argument1.ArgSize);
        }
        else if((disasm->Argument2.ArgType&RELATIVE_)==RELATIVE_)
        {
            basicinfo->type|=TYPE_MEMORY;
            basicinfo->memory.value=(ULONG_PTR)disasm->Instruction.AddrValue;
            strcpy(basicinfo->memory.mnemonic, disasm->Argument2.ArgMnemonic);
            basicinfo->memory.size=argsize2memsize(disasm->Argument2.ArgSize);
        }
    }
}

static void disassemble(void* data, size_t size)
{
    DWORD ticks=GetTickCount();
    printf("disassemble(%p, %X)\n", data, size);
    DISASM disasm;
    memset(&disasm, 0, sizeof(disasm));
#ifdef _WIN64
    disasm.Archi=64;
#endif // _WIN64
    disasm.EIP=(UIntPtr)data;
    disasm.VirtualAddr=(UInt64)data;
    unsigned int i=0;
    while(i<size)
    {
        int len=Disasm(&disasm);
        if(len!=UNKNOWN_OPCODE)
        {
            if(disasm.CompleteInstr[strlen(disasm.CompleteInstr)-1]==' ')
                disasm.CompleteInstr[strlen(disasm.CompleteInstr)-1]=0;
            printf("% 40s\n", disasm.CompleteInstr);
            BASIC_INSTRUCTION_INFO basicinfo;
            fillbasicinfo(&disasm, &basicinfo);
            printbasicinfo(&basicinfo);
        }
        else
            len=1;
        disasm.EIP+=len;
        disasm.VirtualAddr+=len;
        i+=len;
    }
    printf("%ums", GetTickCount()-ticks);
}

int main(int argc, char* argv[])
{
    void* ptr=(void*)main;
    MEMORY_BASIC_INFORMATION mbi;
    memset(&mbi, 0, sizeof(mbi));
    VirtualQuery(ptr, &mbi, sizeof(mbi));
    disassemble(mbi.BaseAddress, mbi.RegionSize);
    return 0;
}
